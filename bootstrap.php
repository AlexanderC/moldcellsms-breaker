<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 1:16 PM
 */

spl_autoload_register(function($class) {
        if(strpos($class, "AlexanderC\\") === 0) {
            $classMap = explode("\\", $class);

            require __DIR__ . '/' . implode("/", $classMap) . ".php";
        }
    });