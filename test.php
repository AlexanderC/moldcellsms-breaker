<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 1:18 PM
 */

require __DIR__ . "/bootstrap.php";

$sender = new \AlexanderC\Moldcell\Sender(__DIR__ . '/cmap'/*, new \AlexanderC\Cache\SharedMemory() */);

$proxyList = new \AlexanderC\Proxy\ProxyList();
$proxies = $proxyList->get();

if(!empty($proxies)) {
    $sender->setProxy($proxies[mt_rand(0, count($proxies) - 1)]);
}

$sms = new \AlexanderC\Moldcell\Sms();
$sms->setText('some text here... sent from proxy ;)');
$sms->setSender('xXx');
$sms->setNumber('78294747');

var_dump($sender->send($sms, 3));
