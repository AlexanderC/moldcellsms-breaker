<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 4:57 PM
 */

namespace AlexanderC\Moldcell;


use AlexanderC\Cache\ACache;
use AlexanderC\Vector\ImagickPixelVector;

class CachedCaptchaBreaker extends CaptchaBreaker
{
    const CACHE_TPL = "__mldclsms_cimgset_%s";

    /**
     * @var ACache
     */
    protected $cacheDriver;

    /**
     * @param ACache $cacheDriver
     */
    public function setCacheDriver(ACache $cacheDriver)
    {
        $this->cacheDriver = $cacheDriver;
    }

    /**
     * @return ACache
     */
    public function getCacheDriver()
    {
        return $this->cacheDriver;
    }

    /**
     * @return array
     */
    protected function getImageset()
    {
        $imageset = null;

        if(null === $imageset) {
            $cacheKey = self::getCacheKey();

            if($this->cacheDriver->has($cacheKey)) {
                $imageset = $this->cacheDriver->get($cacheKey);
            }

            if(!$imageset) {
                $imageset = parent::getImageset();

                $this->cacheDriver->set($cacheKey, $imageset);
            }
        }

        return $imageset;
    }

    /**
     * @return string
     */
    protected static function getCacheKey()
    {
        static $cacheKey = null;

        if(null === $cacheKey) {
            $cacheKey = sprintf(self::CACHE_TPL, sha1(__FILE__));
        }

        return $cacheKey;
    }
} 