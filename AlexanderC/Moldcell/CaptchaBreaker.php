<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 12:43 PM
 */

namespace AlexanderC\Moldcell;


use AlexanderC\Vector\ImagickPixelVector;
use AlexanderC\Vector\VectorCompare;

class CaptchaBreaker
{
    const MAX_COLOR_EUCLID_DISTANCE = 3.33;
    const MAX_SIM_DISTANCE = 1.732;
    const IMAGE_VECTOR_MAP = 'RGB';
    const DEFAULT_RELEVANCE = 0.0;
    const DEFAULT_CHAR = null;

    /**
     * Note: skip 'O' because
     * i guess it is not used at all...
     *
     * @var array
     */
    protected $iconset = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', /*'o',*/
        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    );

    /**
     * @var \Imagick
     */
    protected $image;

    /**
     * @var array
     */
    protected $geometry;

    /**
     * @var string
     */
    protected $storage;

    /**
     * @param string $captchaUrl
     * @param string $storage
     */
    public function __construct($captchaUrl, $storage)
    {
        $this->image = new \Imagick($captchaUrl);
        $this->geometry = $this->image->getImageGeometry();
        $this->storage = realpath($storage) ? : rtrim($storage, '/');
    }

    /**
     * @param bool $dumpLetters
     * @return array
     */
    public function guess($dumpLetters = false)
    {
        $workingImage = $this->getWorkingImage();
        $letters = $this->findLetters($workingImage);
        $imageset = $this->getImageset();

        $guess = array();

        foreach($letters as $letterInfo) {
            $letterImage = clone $workingImage;

            // crop image, but allow delta 1 pixel
            $letterImage->cropImage(
                $letterInfo['x'][1] - $letterInfo['x'][0],
                $letterInfo['y'][1] - $letterInfo['y'][0],
                $letterInfo['x'][0],
                $letterInfo['y'][0]
            );
            $vector = new ImagickPixelVector($letterImage);
            $letterHistogram = $vector->build(self::IMAGE_VECTOR_MAP);

            // uncomment to save image
            if(true === $dumpLetters) {
                $letterImage->writeImage(sprintf("%s/%s.png", $this->storage, uniqid("ld_", true)));
            } else {
                if(!empty($imageset)) {
                    $guess[] = $this->guessChar($letterHistogram, $imageset);
                } else {
                    $guess[] = array(self::DEFAULT_RELEVANCE, self::DEFAULT_CHAR);
                }
            }
        }

        return $guess;
    }

    /**
     * @return array
     */
    protected function getImageset()
    {
        // fill image set
        $imageset = array();

        foreach($this->iconset as $char) {
            $imagesetDir = sprintf("%s/sets/%s", $this->storage, $char);

            if(!is_dir($imagesetDir)) {
                mkdir($imagesetDir, 0777, true);
            }

            // iterate through set images
            foreach(glob(sprintf("%s/*.png", $imagesetDir)) as $charImageFilename) {
                $charImage = new \Imagick($charImageFilename);

                $vector = new ImagickPixelVector($charImage);

                $imageset[] = array($char => $vector->build(self::IMAGE_VECTOR_MAP));
            }
        }

        return $imageset;
    }

    /**
     * @param array $letterHistogram
     * @param array $imageset
     * @return array
     */
    protected function guessChar($letterHistogram, $imageset)
    {
        $guessChar = array();
        $guessResults = array();

        $vc = new VectorCompare();

        foreach($imageset as $charData) {
            $guessChar[] = key($charData);
            $guessResults[] = $vc->relation($letterHistogram, current($charData));
        }

        arsort($guessResults);

        reset($guessResults);
        $resultKey = key($guessResults);

        return array($guessResults[$resultKey], $guessChar[$resultKey]);
    }

    /**
     * @return array
     */
    protected function getWorkingPixelGroup()
    {
        // get pixels
        $matrix = $this->image->getImageHistogram();

        // break pixels into 2 groups
        $pixelGroups = array();

        foreach($matrix as $pixel) {
            if(empty($pixelGroups)) {
                $pixelGroups[0] = array($pixel);
            } else {
                foreach($pixelGroups[0] as $pixel0) {
                    $pdistance = $this->getPixelsDistance($pixel, $pixel0);

                    if($pdistance <= self::MAX_COLOR_EUCLID_DISTANCE) {
                        $pixelGroups[0][] = $pixel;

                        continue 2;
                    }
                }

                if(!isset($pixelGroups[1])) {
                    $pixelGroups[1] = array();
                }

                $pixelGroups[1][] = $pixel;
            }
        }

        // get easiest pixes group
        return $pixelGroups[(count($pixelGroups[0]) < count($pixelGroups[1]) || !isset($pixelGroups[1]))  ? 0 : 1];
    }

    /**
     * @param \ImagickPixel $pixel1
     * @param \ImagickPixel $pixel2
     * @return float
     */
    protected function getPixelsDistance(\ImagickPixel $pixel1, \ImagickPixel $pixel2)
    {
        list($r1, $g1, $b1) = array_values($pixel1->getColor());
        list($r2, $g2, $b2) = array_values($pixel2->getColor());

        return sqrt(pow($r2 - $r1, 2) + pow($g2 - $g1, 2) + pow($b2 - $b1, 2));
    }

    /**
     * @return \Imagick
     */
    protected function getWorkingImage()
    {
        $workingPixelGroup = $this->getWorkingPixelGroup();

        // create similar, but clear image
        $whitePixel = new \ImagickPixel('white');
        $workingImage = new \Imagick();
        $workingImage->newImage($this->geometry['width'], $this->geometry['height'], $whitePixel);
        $workingImage->setImageFormat('png');
        $draw = new \ImagickDraw();
        $draw->setFillColor("black");

        for($x = 0; $x < $this->geometry['width']; $x++) {
            for($y = 0; $y < $this->geometry['height']; $y++) {
                $pixel = $this->image->getImagePixelColor($x, $y);

                foreach($workingPixelGroup as $workingPixel) {
                    if($workingPixel->isSimilar($pixel, 0.0)) {
                        $draw->point($x, $y);
                    }
                }
            }
        }

        $workingImage->drawImage($draw);

        return $workingImage;
    }

    /**
     * @param \Imagick $workingImage
     * @return array
     */
    protected function findLetters(\Imagick $workingImage)
    {
        $whitePixel = new \ImagickPixel('white');

        // find letter vectors -> x,y coords of the letters
        $inLetter = false;
        $foundLetter = false;
        $start = 0;
        $end = 0;

        $letters = array();

        for($x = 0; $x < $this->geometry['width']; $x++) {
            for($y = 0; $y < $this->geometry['height']; $y++) {
                $pixel = $workingImage->getImagePixelColor($x, $y);

                if(!$pixel->isSimilar($whitePixel, 0.0)) {
                    $inLetter = true;
                }
            }

            if(false === $foundLetter && true === $inLetter) {
                $foundLetter = true;
                $start = $x;
            }

            if(true === $foundLetter && false === $inLetter) {
                $foundLetter = false;
                $end = $x;
                $letters[] = array('x' => array($start, $end));
            }

            $inLetter = false;
        }

        foreach($letters as & $letterExtrems) {
            $xCoords = $letterExtrems['x'];

            $letterImage = clone $workingImage;
            $letterImage->cropImage($xCoords[1] - $xCoords[0] + 1, $this->geometry['height'], $xCoords[0], 0);

            $sinLetter = false;
            $sfoundLetter = false;
            $sstart = 0;
            $send = 0;

            for($y = 0; $y < $letterImage->getImageHeight(); $y++) {
                for($x = 0; $x < $letterImage->getImageWidth(); $x++) {
                    $pixel = $letterImage->getImagePixelColor($x, $y);

                    if(!$pixel->isSimilar($whitePixel, 0.0)) {
                        $sinLetter = true;
                    }
                }

                if(false === $sfoundLetter && true === $sinLetter) {
                    $sfoundLetter = true;
                    $sstart = $y;
                }

                if(true === $sfoundLetter && false === $sinLetter) {
                    $sfoundLetter = false;
                    $send = $y;
                    $letterExtrems['y'] = array($sstart, $send);
                }

                $sinLetter = false;
            }
        }

        return $letters;
    }

} 