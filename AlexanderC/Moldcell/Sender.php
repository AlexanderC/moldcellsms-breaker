<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 6:06 PM
 */

namespace AlexanderC\Moldcell;


use AlexanderC\Cache\ACache;
use AlexanderC\Moldcell\Exception\FailedSmsException;
use AlexanderC\Moldcell\Exception\FailingCaptchaException;

class Sender
{
    const TIMEOUT = 100;
    const INPUT = 0x001;
    const TEXTAREA = 0x002;
    const SERVICE_URL = 'http://www.moldcell.md/sendsms';
    const DNAME = 'http://www.moldcell.md/';
    const CAPTHA_CHECK_URL_TPL = "http://www.moldcell.md/rom/mobile/ajax/captcha/validate?text=%s&csid=%s";

    /**
     * @var \AlexanderC\Cache\ACache
     */
    protected $cacheDriver;

    /**
     * @var string
     */
    protected $storage;

    /**
     * @var string
     */
    protected $proxy;

    /**
     * @param string $storage
     * @param ACache $cacheDriver
     */
    public function __construct($storage, ACache $cacheDriver = null)
    {
        $this->cacheDriver = $cacheDriver;
        $this->storage = $storage;
    }

    /**
     * @param string $proxy
     */
    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * @return string
     */
    public function getProxy()
    {
        return $this->proxy;
    }

    /**
     * @param Sms $sms
     * @param int $retry
     * @return bool
     * @throws Exception\FailingCaptchaException
     */
    public function send(Sms $sms, $retry = 3)
    {
        $retry = ((int) $retry) + 1;

        list($formData, $captchaUrl) = $this->getFormInfo();

        $formData['message'] = $sms->getText();
        $formData['name'] = $sms->getSender();
        $formData['phone'] = $sms->getNumber();

        $captchaBreaker = null;

        do {
            $retry--;

            if(null === $this->cacheDriver) {
                $captchaBreaker = new CaptchaBreaker($captchaUrl, $this->storage);
            } else {
                $captchaBreaker = new CachedCaptchaBreaker($captchaUrl, $this->storage);
                $captchaBreaker->setCacheDriver($this->cacheDriver);
            }

            $letters = $captchaBreaker->guess();
            $lettersRaw = array();

            foreach($letters as $letterInfo) {
                $lettersRaw[] = $letterInfo[1];
            }

            $formData['captcha_response'] = implode("", $lettersRaw);

            // check captcha...
            $checkUrl = sprintf(self::CAPTHA_CHECK_URL_TPL, $formData['captcha_response'], $formData['captcha_sid']);
            $checkResponse = @file_get_contents($checkUrl);

            if($checkResponse === '2') {
                break;
            } else {
                $formData['captcha_response'] = null;
            }
        } while($retry > 0);

        if(null === $formData['captcha_response']) {
            throw new FailingCaptchaException("Unable to solve captcha");
        }

        return 302 === $this->sendData($formData);
    }

    /**
     * @param array $data
     * @return int
     */
    protected function sendData(array $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::SERVICE_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);

        if(!empty($this->proxy)) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        }

        $response = curl_exec($ch);
        $returnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $returnCode;
    }

    /**
     * @return array
     */
    protected function getFormInfo()
    {
        $doc = new \DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->loadHTMLFile(self::SERVICE_URL);

        $formData = array();

        $xpath = new \DOMXPath($doc);

        $captcha = $xpath->query("//div[@class='captcha']/img");
        $captcha = $captcha->item(0);
        $captchaUrl = sprintf("%s%s", self::DNAME, ltrim($captcha->getAttribute('src'), "/"));

        $form = $xpath->query("//form[@id='websms-main-form']");
        $form = $form->item(0);

        /** @var \DOMElement $node */
        foreach($this->findFormNodes($form->childNodes) as $node) {
            if($node->localName === 'input' && $node->hasAttribute('name')) {
                $formData[$node->getAttribute('name')] = $node->hasAttribute('value')
                    ? $node->getAttribute('value')
                    : null;
            } elseif($node->localName === 'textarea' && $node->hasAttribute('name')) {
                $formData[$node->getAttribute('name')] = $node->textContent;
            }
        }

        return array($formData, $captchaUrl);
    }

    /**
     * @param \DOMNodeList $nodeList
     * @return array
     */
    protected function findFormNodes(\DOMNodeList $nodeList)
    {
        $nodes = array();

        for($i = 0; $i < $nodeList->length; $i++) {
            $child = $nodeList->item($i);

            if(in_array($child->localName, array('input', 'textarea'))) {
                $nodes[] = $child;
            } elseif($child->hasChildNodes()) {
                $nodes = array_merge($nodes, $this->findFormNodes($child->childNodes));
            }
        }

        return $nodes;
    }
} 