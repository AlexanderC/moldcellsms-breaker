<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 5:46 PM
 */

namespace AlexanderC\Moldcell;


class Sms
{
    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $sender;

    /**
     * @var string
     */
    protected $text;

    /**
     * Note: number -> prefix(ex. 78) + 6 digits
     *
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = preg_replace("/^\+?(373)?0?(\d{8})/u", "$2", $number);
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $sender
     * @throws \InvalidArgumentException
     */
    public function setSender($sender)
    {
        if(strlen($sender) !== mb_strlen($sender)) {
            throw new \InvalidArgumentException("Sender must contain only ASCII characters");
        }

        $this->sender = $sender;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $text
     * @throws \InvalidArgumentException
     */
    public function setText($text)
    {
        $asciilen = strlen($text);
        $mblen = mb_strlen($text);

        if(strlen($text) > 140) {
            throw new \InvalidArgumentException("Message is too large. Max 140 characters allowed");
        }

        if($mblen > $asciilen && $asciilen > 69) {
            throw new \InvalidArgumentException("Message is too large. Max 69 characters allowed when UNICODE used");
        }

        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
} 