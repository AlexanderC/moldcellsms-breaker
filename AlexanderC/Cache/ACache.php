<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 5:05 PM
 */

namespace AlexanderC\Cache;


abstract class ACache
{
    /**
     * @param string $key
     * @return bool
     */
    abstract public function has($key);

    /**
     * @param string $key
     * @return string
     */
    abstract protected function _get($key);

    /**
     * @param string $key
     * @param string $value
     * @return bool
     */
    abstract protected function _set($key, $value);

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function set($key, $value)
    {
        return $this->_set($key, serialize($value));
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return @unserialize($this->_get($key));
    }
} 