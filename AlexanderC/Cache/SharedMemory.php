<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 5:09 PM
 */

namespace AlexanderC\Cache;


class SharedMemory extends ACache
{
    /**
     * @var int
     */
    protected $shmid;

    /**
     * create shared memory block on start
     * 10000bytes memory by default
     */
    public function __construct($memPreserve = 10000)
    {
        $this->shmid = shm_attach(ftok(__FILE__, 'm'), (int) $memPreserve);

        if ($this->shmid === false) {
            throw new \RuntimeException("Unable to create shared memory segment");
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        return shm_has_var($this->shmid, $this->prepareKey($key));
    }

    /**
     * @param string $key
     * @return string
     */
    protected function _get($key)
    {
        return shm_get_var($this->shmid, $this->prepareKey($key));
    }

    /**
     * @param string $key
     * @param string $value
     * @return bool
     */
    protected function _set($key, $value)
    {
        return @shm_put_var($this->shmid, $this->prepareKey($key), $value);
    }

    /**
     * @param $key
     * @return int
     */
    protected function prepareKey($key)
    {
        $len = strlen($key);
        $idString = "1";

        for($i = 0; $i < $len; $i++) {
            $idString .= ord($key{$i});
        }

        return (int) $idString;
    }
} 