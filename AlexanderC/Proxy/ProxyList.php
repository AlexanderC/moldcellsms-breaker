<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 4/1/14
 * @time 10:04 AM
 */

namespace AlexanderC\Proxy;


class ProxyList
{
    const SERVICE_URL = "http://www.google-proxy.net/";

    /**
     * @var array
     */
    protected $proxies = array();

    /**
     * Get proxies on start
     */
    public function __construct()
    {
        $this->refresh();
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->proxies;
    }

    /**
     * @return void
     */
    public function refresh()
    {
        $doc = new \DOMDocument();
        @$doc->loadHTMLFile(self::SERVICE_URL);

        $xpath = new \DOMXPath($doc);

        $serverNodes = $xpath->query("//table[@id='proxylisttable']/tbody/tr");

        for($i = 0; $i < $serverNodes->length; $i++) {
            $tr = $serverNodes->item($i);

            if($tr->childNodes->item(6)->textContent == 'no') { // if not https
                $this->proxies[] = sprintf(
                    "%s:%s",
                    trim($tr->childNodes->item(0)->textContent),
                    trim($tr->childNodes->item(1)->textContent)
                );
            }
        }
    }
} 