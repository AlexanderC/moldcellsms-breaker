<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 12:45 PM
 */

namespace AlexanderC\Vector;


class VectorCompare
{
    /**
     * @param array $concordance
     * @return float
     */
    public function magnitude($concordance)
    {
        $total = 0;

        foreach($concordance as $count) {
            $total += pow($count, 2);
        }

        return sqrt($total);
    }

    /**
     * @param $concordance1
     * @param $concordance2
     * @return float|int
     */
    public function relation($concordance1, $concordance2)
    {
        $topvalue = 0;

        foreach($concordance1 as $word => $count) {
            if(array_key_exists($word, $concordance2)) {
                $topvalue += $count * $concordance2[$word];
            }
        }

        $factor1 = ($this->magnitude($concordance1) * $this->magnitude($concordance2));

        return $topvalue == 0 ? 0 :  ($topvalue / ($factor1 == 0 ? $topvalue : $factor1));
    }
} 