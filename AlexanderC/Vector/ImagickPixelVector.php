<?php
/**
 * @author AlexanderC <self@alexanderc.me>
 * @date 3/31/14
 * @time 12:47 PM
 */

namespace AlexanderC\Vector;


class ImagickPixelVector
{
    /**
     * @var \Imagick
     */
    protected $image;

    /**
     * @param \Imagick $image
     */
    public function __construct(\Imagick $image)
    {
        $this->image = $image;
    }

    /**
     * @param string $map
     * @return array
     */
    public function build($map)
    {
        return $this->image
            ->exportImagePixels(0, 0,
                                $this->image->getImageWidth(),
                                $this->image->getImageHeight(),
                                $map, \Imagick::PIXEL_FLOAT);
    }
} 